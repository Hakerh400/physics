#include "util.hh"

int floor(double x){
  #undef floor
  return static_cast<int>(std::floor(x));
  #define floor _floor_aux
}

int ceil(double x){
  #undef ceil
  return static_cast<int>(std::ceil(x));
  #define ceil _ceil_aux
}

double sqrt(double x){
  #undef sqrt
  return std::sqrt(x);
  #define sqrt _sqrt_aux
}

int bound(int val_min, int val_max, int val){
  if(val < val_min) return val_min;
  if(val > val_max) return val_max;
  return val;
}

double bound(double val_min, double val_max, double val){
  if(val < val_min) return val_min;
  if(val > val_max) return val_max;
  return val;
}

double min(double a, double b){
  if(a < b) return a;
  return b;
}

double max(double a, double b){
  if(a > b) return a;
  return b;
}

double hypot(double x, double y){
  return sqrt(x * x + y * y);
}

double hypot(double x, double y, double z){
  return sqrt(x * x + y * y + z * z);
}

double dist(double x1, double y1, double x2, double y2){
  return hypot(x2 - x1, y2 - y1);
}

double dist(double x1, double y1, double z1, double x2, double y2, double z2){
  return hypot(x2 - x1, y2 - y1, z2 - z1);
}

double pow(double b, double e){
  #undef pow
  return std::pow(b, e);
  #define pow _pow_aux
}

double sin(double x){
  #undef sin
  return std::sin(x);
  #define sin _sin_aux
}

double cos(double x){
  #undef cos
  return std::cos(x);
  #define cos _cos_aux
}

double log_2(double x){
  return std::log2(x);
}

double abs(double x){
  #undef abs
  if(x >= 0) return x;
  return -x;
  #define abs _abs_aux
}

double atan2(double y, double x){
  #undef atan2
  return std::atan2(y, x);
  #define atan2 _atan2_aux
}

double angle_to(double x, double y){
  #undef atan2
  double a = std::atan2(y, x);
  if(a < 0) a += PI2;
  return a;
  #define atan2 _atan2_aux
}

double norm_angle(double a){
  while(a > PI) a -= PI2;
  while(a < -PI) a += PI2;
  return a;
}