#include "util.hh"
#include "vector.hh"

const Vector Vector::LEFT = Vector(-1, 0, 0);
const Vector Vector::RIGHT = Vector(1, 0, 0);
const Vector Vector::UP = Vector(0, -1, 0);
const Vector Vector::DOWN = Vector(0, 1, 0);
const Vector Vector::FRONT = Vector(0, 0, -1);
const Vector Vector::BACK = Vector(0, 0, 1);

Vector::Vector(){
  x = 0;
  y = 0;
  z = 0;
}

Vector::Vector(double x, double y, double z){
  this->x = x;
  this->y = y;
  this->z = z;
}

double Vector::length() const {
  return hypot(x, y, z);
}

double Vector::dist(const Vector& v) const {
  return ::dist(x, y, z, v.x, v.y, v.z);
}

Vector Vector::norm() const {
  double len = length();
  if(len == 0) return Vector::RIGHT;
  return *this / len;
}

Vector Vector::to(const Vector& v) const {
  return v - *this;
}

Vector Vector::operator+(const Vector& v) const {
  return Vector(x + v.x, y + v.y, z + v.z);
}

Vector& Vector::operator+=(const Vector& v){
  x += v.x;
  y += v.y;
  z += v.z;
  return *this;
}

Vector Vector::operator-() const {
  return Vector(-x, -y, -z);
}

Vector Vector::operator-(const Vector& v) const {
  return Vector(x - v.x, y - v.y, z - v.z);
}

Vector& Vector::operator-=(const Vector& v){
  x -= v.x;
  y -= v.y;
  z -= v.z;
  return *this;
}

Vector Vector::operator*(double k) const {
  return Vector(x * k, y * k, z * k);
}

Vector& Vector::operator*=(double k){
  x *= k;
  y *= k;
  z *= k;
  return *this;
}

Vector Vector::operator/(double k) const {
  return Vector(x / k, y / k, z / k);
}

Vector& Vector::operator/=(double k){
  x /= k;
  y /= k;
  z /= k;
  return *this;
}

double Vector::operator[](int i) const {
  if(i == 0) return x;
  if(i == 1) return y;
  if(i == 2) return z;
  assert(0);
}

std::string Vector::to_string() const {
  return "(" + std::to_string(x) +
    ", " + std::to_string(y) + ")";
}

Vector Vector::get_angle() const {
  // return atan2(y, x);
  return norm();
}

Vector Vector::angle_to(const Vector& b) const {
  // const Vector& a = *this;
  // return atan2(b.y - a.y, b.x - a.x);
  return (b - *this).get_angle();
}