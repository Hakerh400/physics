#include "log.hh"
#include "bond.hh"

Bond::Bond(Particle* p1, Particle* p2, double target_len){
  assert(p1 != p2);

  particle1 = p1;
  particle2 = p2;

  p1->add_bond(this);
  p2->add_bond(this);

  this->target_len = target_len;
}

Bond::Bond(Particle* p1, Particle* p2){
  assert(p1 != p2);
  // Bond(p1, p2, p1->dist(p2));

  particle1 = p1;
  particle2 = p2;

  p1->add_bond(this);
  p2->add_bond(this);

  // log(p1->dist(p2));

  this->target_len = p1->dist(p2);
}

Bond::~Bond(){
  remove_particles();
}

Particle* Bond::get_particle1(){
  return particle1;
}

Particle* Bond::get_particle2(){
  return particle2;
}

Vector Bond::get_dir1() const {
  return particle1->get_position().to(particle2->get_position());
}

Vector Bond::get_dir2() const {
  return particle2->get_position().to(particle1->get_position());
}

bool Bond::has(Particle* p) const {
  return p == particle1 || p == particle2;
}

Particle* Bond::get_other(Particle* p){
  if(p == particle1) return particle2;
  if(p == particle2) return particle1;
  return nullptr;
}

void Bond::remove_particles(){
  if(particle1 != nullptr){
    particle1->remove_bond(this);
    particle1 = nullptr;
  }

  if(particle2 != nullptr){
    particle2->remove_bond(this);
    particle2 = nullptr;
  }
}

double Bond::length() const {
  return particle1->dist(particle2);
}

double Bond::get_target_len() const {
  return target_len;
}

void Bond::set_target_len(double target_len){
  this->target_len = target_len;
}

Vector Bond::get_angle() const {
  return particle1->get_position().
    angle_to(particle2->get_position());
}