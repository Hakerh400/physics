#pragma once

#include "util.hh"

class Color {
public:
  static const Color& WHITE;
  static const Color& BLACK;

  Color();
  Color(double, double, double);
  const double* get_rgb() const;
  double get_red() const;
  double get_green() const;
  double get_blue() const;
  void set_red(double);
  void set_green(double);
  void set_blue(double);
  double operator[](int) const;

private:
  double rgb[3];
};