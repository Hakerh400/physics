#pragma once

#include "rigid_body.hh"

class Simulator {
public:
  Simulator(std::string, int, int, int);
  ~Simulator();
  int get_width() const;
  int get_height() const;
  int get_depth() const;
  int get_grid_size() const;
  const Cell* get_grid() const;
  const Cell& get_grid_cell_c(int) const;
  const Cell& get_grid_cell_c(int, int, int) const;
  const Cell& get_grid_cell_c(double, double, double) const;
  const Cell& get_grid_cell_c(const Vector&) const;
  const std::set<Particle*>& get_particles() const;
  void add_particle(Particle*);
  void remove_particle(Particle*);
  bool has_bond(Particle*, Particle*) const;
  Bond* get_bond(Particle*, Particle*);
  void add_bond(Bond*);
  Bond* add_bond(Particle*, Particle*, double);
  Bond* add_bond(Particle*, Particle*);
  void add_rigid_body(RigidBody*);
  void remove_bond(Bond*);
  void remove_bond(Particle*, Particle*);
  void tick(double x=1);
  int get_last_iters_num() const;
  void update_particle_cell(Particle*);
  // void clear_grid();
  // void update_grid();

private:
  int w, h, d;
  int grid_size;
  std::set<Particle*> particles;
  std::set<Bond*> bonds;
  std::set<RigidBody*> rigid_bodies;
  Cell* grid;
  int last_iters_num;
  std::set<Particle*> removed_particles;
  std::set<Bond*> removed_bonds;
  
  Cell& get_grid_cell(int);
  Cell& get_grid_cell(int, int, int);
  Cell& get_grid_cell(double, double, double);
  Cell& get_grid_cell(const Vector&);
  double get_max_particle_velocity() const;
  void tick_aux(double);
  void update_removed();
};