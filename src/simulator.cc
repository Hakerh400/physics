#include "util.hh"
#include "log.hh"
#include "process.hh"
#include "simulator.hh"

Simulator::Simulator(std::string pth, int w, int h, int d){
  this->w = w;
  this->h = h;
  this->d = d;
  grid_size = w * h * d;
  grid = new Cell[grid_size];
  last_iters_num = 0;
}

Simulator::~Simulator(){
  for(int i = 0; i != grid_size; i++)
    grid[i].clear();
  
  delete[] grid;

  for(Particle* p : particles)
    delete p;

  particles.clear();
  removed_particles.clear();
}

int Simulator::get_width() const {
  return w;
}

int Simulator::get_height() const {
  return h;
}

int Simulator::get_grid_size() const {
  return grid_size;
}

const Cell* Simulator::get_grid() const {
  return grid;
}

const std::set<Particle*>& Simulator::get_particles() const {
  return particles;
}

void Simulator::add_particle(Particle* p){
  Cell& cell = get_grid_cell(p->get_position());
  
  particles.insert(p);
  cell.insert(p);
  p->set_cell(&cell);
}

void Simulator::remove_particle(Particle* p){
  removed_particles.insert(p);
}

void Simulator::add_bond(Bond* bond){
  bonds.insert(bond);
}

Bond* Simulator::add_bond(Particle* p1, Particle* p2, double len){
  Bond* bond = new Bond(p1, p2, len);
  add_bond(bond);
  return bond;
}

Bond* Simulator::add_bond(Particle* p1, Particle* p2){
  Bond* bond = new Bond(p1, p2);
  add_bond(bond);
  return bond;
}

void Simulator::add_rigid_body(RigidBody* body){
  rigid_bodies.insert(body);
}

void Simulator::remove_bond(Bond* bond){
  removed_bonds.insert(bond);
}

bool Simulator::has_bond(Particle* p1, Particle* p2) const {
  return p1->has_bond(p2);
}

Bond* Simulator::get_bond(Particle* p1, Particle* p2){
  return p1->get_bond(p2);
}

void Simulator::remove_bond(Particle* p1, Particle* p2){
  Bond* bond = get_bond(p1, p2);
  remove_bond(bond);
}

/////

const Cell& Simulator::get_grid_cell_c(int i) const {
  return grid[i];
}

const Cell& Simulator::get_grid_cell_c(int x, int y, int z) const {
  x = bound(0, w - 1, x);
  y = bound(0, h - 1, y);
  z = bound(0, d - 1, z);
  return get_grid_cell_c((z * h + y) * w + x);
}

const Cell& Simulator::get_grid_cell_c(double x, double y, double z) const {
  return get_grid_cell_c(floor(x), floor(y), floor(z));
}

const Cell& Simulator::get_grid_cell_c(const Vector& v) const {
  return get_grid_cell_c(v[0], v[1], v[2]);
}

/////

Cell& Simulator::get_grid_cell(int i){
  return const_cast<Cell&>(get_grid_cell_c(i));
}

Cell& Simulator::get_grid_cell(int x, int y, int z){
  return const_cast<Cell&>(get_grid_cell_c(x, y, z));
}

Cell& Simulator::get_grid_cell(double x, double y, double z){
  return const_cast<Cell&>(get_grid_cell_c(x, y, z));
}

Cell& Simulator::get_grid_cell(const Vector& v){
  return const_cast<Cell&>(get_grid_cell_c(v));
}

/////

// void Simulator::clear_grid(){
//   for(int i = 0; i != grid_size; i++){
//     Cell& set = grid[i];
//     set.clear();
//   }
// }

// void Simulator::update_grid(){
//   this->clear_grid();
// 
//   for(Particle* p : particles)
//     get_grid_cell(p->get_position()).insert(p);
// }

double Simulator::get_max_particle_velocity() const {
  double max_velocity = 0;

  for(Particle* p : particles){
    double velocity = p->get_velocity().length();

    if(velocity > max_velocity)
      max_velocity = velocity;
  }
  
  return max_velocity;
}

void Simulator::tick_aux(double t){
  update_removed();

  for(Particle* p : particles){
    if(p->is_static()) continue;
    
    p->move(t);
    p->apply_friction(t);
    p->apply_gravity(t);

    const Vector& pos = p->get_position();
    double x = pos[0];
    double y = pos[1];
    double z = pos[2];

    if(x < 0){
      p->apply_collision_force(Vector::RIGHT, -x, t);
    }else if(x > w){
      p->apply_collision_force(Vector::LEFT, x - w, t);
    }

    if(y < 0){
      p->apply_collision_force(Vector::DOWN, -y, t);
    }else if(y > h){
      p->apply_collision_force(Vector::UP, y - h, t);
    }

    if(z < 0){
      p->apply_collision_force(Vector::BACK, -z, t);
    }else if(z > d){
      p->apply_collision_force(Vector::FRONT, z - d, t);
    }
  }

  for(Particle* p : particles)
    update_particle_cell(p);

  for(Particle* p : particles){
    if(p->is_static()) continue;

    const Vector& pos = p->get_position();
    double px = pos[0];
    double py = pos[1];
    double pz = pos[2];
    int x = bound(0, w - 1, floor(px));
    int y = bound(0, h - 1, floor(py));
    int z = bound(0, d - 1, floor(pz));

    int x_start = bound(0, w - 1, floor(px - PARTICLE_DIAMETER));
    int y_start = bound(0, h - 1, floor(py - PARTICLE_DIAMETER));
    int z_start = bound(0, d - 1, floor(pz - PARTICLE_DIAMETER));
    int x_end = bound(0, w - 1, floor(px + PARTICLE_DIAMETER));
    int y_end = bound(0, h - 1, floor(py + PARTICLE_DIAMETER));
    int z_end = bound(0, d - 1, floor(pz + PARTICLE_DIAMETER));

    for(int z1 = z_start; z1 <= z_end; z1++){
      for(int y1 = y_start; y1 <= y_end; y1++){
        for(int x1 = x_start; x1 <= x_end; x1++){
          for(Particle* p1 : get_grid_cell(x1, y1, z1)){
            if(p1 == p) continue;

            double d = p->dist(p1);
            if(d >= PARTICLE_DIAMETER) continue;
            
            if(p->is_heavy() && !p1->is_heavy())
              continue;

            if(!p->is_sharp() && p1->is_sharp())
              for(Bond* b : p->get_bonds())
                remove_bond(b);

            Vector dir = p1->get_position().to(pos).norm();
            double k = 1 - d / PARTICLE_DIAMETER;

            // p->apply_collision_force(dir, k, t);
            p->apply_collision_force(dir, k, 1);
          }
        }
      }
    }
  }

  // for(Particle* p : particles)
  //   p->set_velocity(p->velocity_new);

  // double x = 0;

  for(Bond* bond : bonds){
    double len = bond->length();
    if(len < 1) continue;

    // double d0 = len - 1;
    // double d = d0 / PARTICLE_DIAMETER;
    // double k = d0 / 10;

    // if(len > x) x = len;

    // if(len > 10){
    //   remove_bond(bond);
    //   continue;
    // }

    // double k = log_2(len);

    Particle* p1 = bond->get_particle1();
    Particle* p2 = bond->get_particle2();
    Vector dir = bond->get_dir1();
    double tlen = bond->get_target_len();

    // if(len > tlen * 3){
    //   remove_bond(bond);
    //   continue;
    // }

    {
      double k = len / tlen;

      if(k > 2.5){
        remove_bond(bond);
        continue;
      }
      
      // if(k > z){
      //   z = k;
      //   log("\n>>> ", 0);
      //   log(z);
      // }
    }

    double k = (len - tlen) / PARTICLE_DIAMETER;
    dir = dir.norm();

    p1->apply_collision_force(dir, k, t);
    p2->apply_collision_force(-dir, k, t);

    // p1->apply_force(dir, t);
    // p2->apply_force(-dir, t);
  }

  // log(x);
}

void Simulator::tick(double time){
  int iters_num = 0;

  while(time > 0){
    double max_velocity = get_max_particle_velocity();
    double t_max = MAX_MOVE_DIST / max_velocity;
    double t = min(time, t_max);

    tick_aux(t);
    time -= t;

    iters_num++;
  }

  // for(RigidBody* body : rigid_bodies){
  //   body->update();
  // }

  last_iters_num = iters_num;
}

int Simulator::get_last_iters_num() const {
  return last_iters_num;
}

void Simulator::update_removed(){
  for(Particle* p : removed_particles){
    for(Bond* bond : p->get_bonds())
      remove_bond(bond);

    particles.erase(p);
    get_grid_cell(p->get_position()).remove(p);
  }

  for(Bond* bond : removed_bonds){
    bonds.erase(bond);
    delete bond;
  }

  for(Particle* p : removed_particles)
    delete p;
  
  removed_particles.clear();
  removed_bonds.clear();
}

void Simulator::update_particle_cell(Particle* p){
  Cell* cell0 = p->get_cell();
  Cell* cell = &get_grid_cell(p->get_position());
  if(cell == cell0) return;
  
  cell0->remove(p);
  cell->insert(p);
  p->set_cell(cell);
}