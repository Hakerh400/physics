#pragma once

#include "util.hh"
#include "log.hh"

template<typename T, int max_len>
class Array {
public:
  Array();
  void clear();
  T* begin() const;
  T* end() const;
  int length() const;
  bool is_empty() const;
  void insert(const T&);
  void remove(const T&);

private:
  int len;
  T data[max_len];
};

template<typename T, int max_len>
Array<T, max_len>::Array(){
  assert(max_len > 0);
  len = 0;
}

template<typename T, int max_len>
void Array<T, max_len>::clear(){
  len = 0;
}

template<typename T, int max_len>
T* Array<T, max_len>::begin() const {
  return (T*)data;
}

template<typename T, int max_len>
T* Array<T, max_len>::end() const {
  return (T*)data + len;
}

template<typename T, int max_len>
int Array<T, max_len>::length() const {
  return len;
}

template<typename T, int max_len>
bool Array<T, max_len>::is_empty() const {
  return len == 0;
}

template<typename T, int max_len>
void Array<T, max_len>::insert(const T& x){
  assert(len != max_len);
  data[len++] = x;
}

template<typename T, int max_len>
void Array<T, max_len>::remove(const T& x){
  for(int i = 0; i != len; i++){
    if(data[i] != x) continue;
    
    len--;
    
    for(int j = i; j != len; j++)
      data[j] = data[j + 1];
    
    return;
  }
  
  // assert(0);
}