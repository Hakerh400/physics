#pragma once

#include "util.hh"

class Vector {
public:
  static const Vector LEFT;
  static const Vector RIGHT;
  static const Vector UP;
  static const Vector DOWN;
  static const Vector FRONT;
  static const Vector BACK;

  Vector();
  Vector(double x, double y, double z);
  double length() const;
  double dist(const Vector&) const;
  Vector norm() const;
  Vector to(const Vector&) const;
  Vector operator+(const Vector&) const;
  Vector& operator+=(const Vector&);
  Vector operator-() const;
  Vector operator-(const Vector&) const;
  Vector& operator-=(const Vector&);
  Vector operator*(double) const;
  Vector& operator*=(double);
  Vector operator/(double) const;
  Vector& operator/=(double);
  double operator[](int) const;
  std::string to_string() const;
  Vector get_angle() const;
  Vector angle_to(const Vector&) const;

private:
  double x, y, z;
};