#pragma once

#include "util.hh"
#include "vector.hh"

template<typename T>
void log_aux(T, bool endl=1);

void log();
void log(const char*, bool endl=1);
void log(char*, bool endl=1);
void log(int, bool endl=1);
void log(uint32_t, bool endl=1);
void log(uint64_t, bool endl=1);
void log(double, bool endl=1);
void log(std::string, bool endl=1);
void log(Vector, bool endl=1);
void log(void*, bool endl=1);

void logb();