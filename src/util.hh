#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <functional>

#define PI ((double)3.141592653589793)
#define PI2 ((double)(PI * 2))

#define log _log_aux
#define floor _floor_aux
#define ceil _ceil_aux
#define sqrt _sqrt_aux
#define min _min_aux
#define max _max_aux
#define hypot _hypot_aux
#define dist _dist_aux
#define pow _pow_aux
#define sin _sin_aux
#define cos _cos_aux
#define atan2 _atan2_aux
#define abs _abs_aux

int floor(double);
int ceil(double);
double sqrt(double);
int bound(int, int, int);
double bound(double, double, double);
double min(double, double);
double max(double, double);
double hypot(double, double);
double hypot(double, double, double);
double dist(double, double, double, double);
double dist(double, double, double, double, double, double);
double pow(double, double);
double sin(double);
double cos(double);
double log_2(double);
double abs(double);
double atan2(double, double);
double angle_to(double, double);
double norm_angle(double);