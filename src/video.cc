#include "util.hh"
#include "log.hh"
#include "process.hh"
#include "video.hh"

#ifdef FLAG_LINUX
  const char* MODE_READ = "r";
  const char* MODE_WRITE = "w";
#endif
#ifdef FLAG_WINDOWS
  const char* MODE_READ = "rb";
  const char* MODE_WRITE = "wb";
#endif

Video::Video(const char* pth, int w, int h){
  this->pth = pth;
  this->width = w;
  this->height = h;
  this->pixels_num = w * h;
  this->imgd = new uint8_t[this->pixels_num << 2];
  std::string wh_str = std::to_string(w) + "x" + std::to_string(h);
  std::string cmd = "ffmpeg";
  std::string args[] = {
    "-hide_banner",
    "-loglevel", "error",
    "-f", "rawvideo",
    "-pix_fmt", "rgba",
    "-s", wh_str,
    "-framerate", "60",
    "-i", "-",
    "-y",
    "-framerate", "60",
    "-preset", "slow",
    "-profile:v", "high",
    "-coder", "1",
    "-movflags", "+faststart",
    "-bf", "2",
    "-max_muxing_queue_size", "100000",
    "-crf", "15",
    "-pix_fmt", "nv12",
    pth,
    #ifdef FLAG_LINUX
      "> /dev/null 2>&1",
    #endif
    #ifdef FLAG_WINDOWS
      "> NUL 2>&1",
    #endif
  };
  for(std::string arg : args)
    cmd += " " + arg;
  proc = popen(cmd.c_str(), MODE_WRITE);
  valid = proc != nullptr;
}

Video::~Video(){
  delete imgd;
  if(proc) this->finish();
}

bool Video::is_valid(){
  return valid;
}

int Video::get_pixels_num(){
  assert(valid);
  return pixels_num;
}

uint8_t* Video::get_imgd(){
  assert(valid);
  return imgd;
}

void Video::add_frame(){
  assert(valid);
  fwrite(imgd, 4, pixels_num, proc);
  fflush(proc);
}

void Video::finish(){
  assert(valid);
  pclose(proc);
  proc = nullptr;
}