#pragma once

#include "particle.hh"

class Bond {
public:
  Bond(Particle*, Particle*, double);
  Bond(Particle*, Particle*);
  ~Bond();
  Particle* get_particle1();
  Particle* get_particle2();
  Vector get_dir1() const;
  Vector get_dir2() const;
  bool has(Particle*) const;
  Particle* get_other(Particle*);
  void remove_particles();
  double length() const;
  double get_target_len() const;
  void set_target_len(double);
  Vector get_angle() const;

private:
  Particle* particle1;
  Particle* particle2;
  double target_len;
};