#include "color.hh"

const Color& Color::WHITE = Color(1, 1, 1);
const Color& Color::BLACK = Color(0, 0, 0);

Color::Color(){
  rgb[0] = 0;
  rgb[1] = 0;
  rgb[2] = 0;
}

Color::Color(double red, double green, double blue){
  rgb[0] = bound(0.0, 1.0, red);
  rgb[1] = bound(0.0, 1.0, green);
  rgb[2] = bound(0.0, 1.0, blue);
}

const double* Color::get_rgb() const {
  return rgb;
}

double Color::get_red() const {
  return rgb[0];
}

double Color::get_green() const {
  return rgb[1];
}

double Color::get_blue() const {
  return rgb[2];
}

void Color::set_red(double red){
  rgb[0] = red;
}

void Color::set_green(double green){
  rgb[1] = green;
}

void Color::set_blue(double blue){
  rgb[2] = blue;
}

double Color::operator[](int i) const {
  assert(i >= 0 && i < 3);
  return rgb[i];
}