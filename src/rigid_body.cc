#include "log.hh"
#include "simulator.hh"

/*RigidBody::RigidBody(Simulator* sim){
  this->sim = sim;

  finalized = 0;
  angle = 0;
  particles_num = 0;
}

void RigidBody::add_particle(Particle* p){
  assert(!finalized);
  particles.insert(p);
  particles_num++;
}

bool RigidBody::is_finalized() const {
  return finalized;
}

void RigidBody::finalize(){
  assert(!finalized);
  assert(particles_num != 0);

  update_com();

  for(Particle* p : particles){
    Vector v = p->get_position() - com;

    ps_lengths[p] = v.length();
    ps_angles[p] = v.get_angle();
  }

  finalized = 1;
}

double RigidBody::get_angle() const {
  return angle;
}

void RigidBody::update_com(){
  com = Vector(0, 0);

  for(Particle* p : particles)
    com = com + p->get_position();

  com = com / particles_num;
}

void RigidBody::update(){
  update_com();

  double angle_dif = 0;

  for(Particle* p : particles){
    double t = angle + ps_angles.at(p);
    double a = com.angle_to(p->get_position());
    double da1 = a - t;
    double da2 = a + PI2 - t;
    double da3 = a - PI2 - t;
    double da1b = abs(da1);
    double da2b = abs(da2);
    double da3b = abs(da3);
    double da = min(da1b, min(da2b, da3b));

    angle_dif += da == da1b ? da1 :
      da == da2b ? da2 : da3;
  }

  angle_dif /= particles_num;
  angle = norm_angle(angle + angle_dif);

  for(Particle* p : particles){
    double r = ps_lengths.at(p);
    double a = angle + ps_angles.at(p);

    Vector pos = com + Vector(cos(a), sin(a)) * r;
    p->set_position(pos);
    sim->update_particle_cell(p);
  }
}*/