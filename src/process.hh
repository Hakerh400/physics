#pragma once

// enum ProcStatus {
//   initialized,
//   started,
//   ended,
// };

// struct Handle {
//   void* handle;
// };

// struct IOHandle {
//   Handle read;
//   Handle write;
// };

// class Process {
// public:
//   char* cmd;
//   char** args;
  
//   Process();
//   ~Process();
  
//   bool has_args();
//   int args_num();
//   ProcStatus get_status();

// private:
//   ProcStatus status;
//   Handle* proc;
//   IOHandle* stdin_rw;
//   IOHandle* stdout_rw;
//   IOHandle* stderr_rw;
// };