#include "main.hh"
#include "util.hh"
#include "log.hh"
#include "process.hh"
#include "video.hh"
#include "simulator.hh"
#include "particle.hh"

uint64_t seed = 0;

void main1(){
  // int iw = 1920;
  // int ih = 1080;
  int iw = 400;
  int ih = 300;

  int w = 200;
  int h = 200;
  int d = 30;

  std::ofstream file {"/home/user/Git/test/5.txt"};
  
  #ifdef FLAG_LINUX
    std::string pth = "/home/user/Git/Render/5.mp4";
  #endif
  #ifdef FLAG_WINDOWS
    std::string pth = "D:/Render/1.mp4";
  #endif
  int frames_num = 60 * 60 * 10;
  
  Simulator* simulator = new Simulator(pth.c_str(), w, h, d);
  Video* video = new Video(pth.c_str(), iw, ih);

  if(!video->is_valid()){
    log("Cannot spawn process");
    return;
  }

  uint8_t* data = video->get_imgd();
  double* zbuf = new double[iw * ih];

  for(int frame = 1; frame <= frames_num; frame++){
    log("Processing frame " +
      std::to_string(frame) +
      " of " + std::to_string(frames_num) + " ...", 0);

    int fn = 60;
    // if(frame == 1){
    if(frame % fn == 1){
      int k = frame / fn % 6;

      int ws = 10;
      int hs = 10;
      int ds = 10;

      Particle** grid = new Particle*[ws * hs * ds];

      for(int z = 0; z != ds; z++){
        for(int y = 0; y != hs; y++){
          for(int x = 0; x != ws; x++){
            int i = (z * hs + y) * ws + x;

            grid[i] = nullptr;

            Vector pos = Vector(
              0.5 + 10 + x + ((1 + sin(1 + x - y + z)) / 1e3),
              0.5 + 10 + y + ((2 + cos(1 + x + y - z * 1.07)) / 1e3),
              0.5 + 10 + z + ((2 + cos(1 - x + y + z * 1.07)) / 1e3)
            );
            Vector vel = Vector(0, 0, 0);
            Particle* p = new Particle(pos, vel);
            Color col = (
              k == 0 ? Color(1, 0, 0) :
              k == 1 ? Color(1, 1, 0) :
              k == 2 ? Color(0, 1, 0) :
              k == 3 ? Color(0, 1, 1) :
              k == 4 ? Color(0.5, 0.5, 1) :
              k == 5 ? Color(1, 0, 1) :
                Color(1, 1, 1)
            );
            p->set_color(col);
            simulator->add_particle(p);

            grid[i] = p;
          }
        }
      }

      if(1){
        for(int z = 0; z != ds; z++){
          for(int y = 0; y != hs; y++){
            for(int x = 0; x != ws; x++){
              int i = (z * hs + y) * ws + x;
              Particle* p = grid[i];
              if(p == nullptr) continue;

              int x_start = max(0, x - 1);
              int y_start = max(0, y - 1);
              int z_start = max(0, z - 1);
              int x_end = min(ws - 1, x + 1);
              int y_end = min(hs - 1, y + 1);
              int z_end = min(ds - 1, z + 1);

              for(int z1 = z_start; z1 <= z_end; z1++){
                for(int y1 = y_start; y1 <= y_end; y1++){
                  for(int x1 = x_start; x1 <= x_end; x1++){
                    int i = (z1 * hs + y1) * ws + x1;
                    Particle* p1 = grid[i];
                    if(p1 == nullptr) continue;
                    if(reinterpret_cast<void*>(p1) <=
                      reinterpret_cast<void*>(p)) continue;
                    
                    simulator->add_bond(p, p1);
                  }
                }
              }
            }
          }
        }
      }

      delete[] grid;
    }

    if(frame != 1) simulator->tick(1);

    int iters_num = simulator->get_last_iters_num();
    std::string iters_num_str = std::to_string(iters_num);
    log(" Iterations: " + iters_num_str);
    if(frame != 1) file << "\n";
    file << iters_num_str;
    file.flush();

    for(int y = 0, i = 0; y != ih; y++){
      for(int x = 0; x != iw; x++, i += 4){
        data[i] = 0x20;
        data[i + 1] = 0x20;
        data[i + 2] = 0x20;
        zbuf[i >> 2] = d * 2;
      }
    }

    for(const Particle* p : simulator->get_particles()){
      const Vector& pos = p->get_position();
      const Color& col = p->get_color();

      double x = pos[0];
      double y = pos[1];
      double z = pos[2];

      int xn = bound(0, iw - 1, floor((iw - w - d) * 0.5 + x + z));
      int yn = bound(0, ih - 1, floor((ih - h + d) * 0.5 + y - z));
      int i = yn * iw + xn;

      if(z >= zbuf[i]) continue;
      zbuf[i] = z;

      i <<= 2;

      for(int j = 0; j != 3; j++)
        data[i + j] = col[j] * 255;
    }

    video->add_frame();
  }

  file.close();
  video->finish();
  
  delete zbuf;
  delete video;
  delete simulator;
}

int main(){
  if(seed != 0){
    if(seed == 1){
      int* a = new int;
      seed = reinterpret_cast<uint64_t>(a);
      delete a;

      log(seed);
      log();
    }

    srand(seed);
  }

  main1();
  return 0;
}