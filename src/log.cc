#include "log.hh"

template<typename T>
void log_aux(T x, bool endl){
  std::cout << x;
  if(endl) std::cout << std::endl;
  fflush(stdout);
}

void log(){
  log_aux("");
}

void log(const char* s, bool endl){
  log_aux(s, endl);
}

void log(char* s, bool endl){
  log_aux(s, endl);
}

void log(int n, bool endl){
  log_aux(n, endl);
}

void log(uint32_t n, bool endl){
  log_aux(n, endl);
}

void log(uint64_t n, bool endl){
  log_aux(n, endl);
}

void log(double x, bool endl){
  log_aux(x, endl);
}

void log(std::string s, bool endl){
  log_aux(s, endl);
}

void log(Vector vec, bool endl){
  log_aux(vec.to_string(), endl);
}

void log(void* ptr, bool endl){
  log_aux(ptr, endl);
}

void logb(){
  log("");
  
  for(int i = 0; i != 100; i++)
    log("=", 0);
  
  log("\n");
}