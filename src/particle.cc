#include "log.hh"
#include "bond.hh"
#include "particle.hh"

const Vector GRAVITY = Vector(0, 0.01, 0);
// const Vector GRAVITY = Vector(0, 0, 0);

Particle::Particle(Vector position){
  Particle(position, Vector());
}

Particle::Particle(Vector position, Vector velocity){
  this->mass = 1;
  this->position = position;
  this->velocity = velocity;
  this->color = Color::WHITE;
  this->_static = 0;
  this->sharp = 0;
  this->heavy = 0;
}

Particle::~Particle(){
  bonds.clear();
}

bool Particle::is_static() const {
  return _static;
}

double Particle::get_mass() const {
  return mass;
}

const Vector& Particle::get_position() const {
  return this->position;
}

const Vector& Particle::get_velocity() const {
  return this->velocity;
}

const Color& Particle::get_color() const {
  return this->color;
}

void Particle::set_static(bool _static){
  this->_static = _static;
}

void Particle::set_mass(double mass){
  this->mass = mass;
}

void Particle::set_position(Vector position){
  this->position = position;
}

void Particle::set_velocity(Vector velocity){
  this->velocity = velocity;
}

void Particle::set_color(Color color){
  this->color = color;
}

void Particle::move(double t){
  position += velocity * t;
}

void Particle::apply_friction(double t){
  if(_static) return;
  velocity *= 1 - FRICTION * t;
}

void Particle::apply_gravity(double t){
  if(_static) return;
  velocity += GRAVITY * t;
}

void Particle::apply_force(Vector accel, double t){
  if(_static) return;
  velocity += accel * t;

  // if(velocity.length() >= 3){
  //   log(position);
  //   exit(0);
  // }
}

void Particle::apply_collision_force(const Vector& v, double k, double t){
  if(_static) return;
  
  // k /= PARTICLE_DIAMETER;

  // if(k >= 0.75){
  //   logb();
  //   log(k);
  //   exit(1);
  // }

  int sign = 1;

  if(k < 0){
    // k = -k;
    // sign = -1;
    k = 0;
  }

  k = min(1.0, k);
  k = pow(2.0, k) - 1.0;

  // k = 1 * (1 / pow(0.5 - k, 1.5) - 2);

  apply_force(v * (sign * k * 10), t);
}

double Particle::dist(Particle* p) const {
  return position.dist(p->position);
}

void Particle::add_bond(Bond* bond){
  bonds.insert(bond);
}

void Particle::remove_bond(Bond* bond){
  bonds.erase(bond);
}

bool Particle::has_bond(Particle* p) const {
  for(Bond* bond : bonds)
    if(bond->has(p))
      return 1;
  
  return 0;
}

Bond* Particle::get_bond(Particle* p){
  for(Bond* bond : bonds)
    if(bond->has(p))
      return bond;
  
  return nullptr;
}

const std::set<Bond*>& Particle::get_bonds() const {
  return bonds;
}

Cell* Particle::get_cell() const {
  return cell;
}

void Particle::set_cell(Cell* cell){
  this->cell = cell;
}

bool Particle::is_sharp() const {
  return sharp;
}

void Particle::set_sharp(bool sharp){
  this->sharp = sharp;
}

bool Particle::is_heavy() const {
  return heavy;
}

void Particle::set_heavy(bool heavy){
  this->heavy = heavy;
}