#pragma once

#include "array.hh"
#include "vector.hh"
#include "color.hh"

#define CELL_MAX_LEN 15
#define PARTICLE_RADIUS ((double)(0.49))
#define PARTICLE_DIAMETER ((double)(PARTICLE_RADIUS * 2))
#define MAX_MOVE_DIST_FACTOR ((double)(0.01))
// #define MAX_MOVE_DIST_FACTOR ((double)(0.1))
// #define MAX_MOVE_DIST_FACTOR ((double)(0.00025))
#define MAX_MOVE_DIST ((double)(PARTICLE_RADIUS * MAX_MOVE_DIST_FACTOR))
#define FRICTION ((double)(0.005))

class Bond;
class Particle;
class RigidBody;

typedef Array<Particle*, CELL_MAX_LEN> Cell;

extern const Vector GRAVITY;

class Particle {
public:
  Particle(Vector);
  Particle(Vector, Vector);
  ~Particle();
  bool is_static() const;
  double get_mass() const;
  const Vector& get_position() const;
  const Vector& get_velocity() const;
  const Color& get_color() const;
  void set_static(bool);
  void set_mass(double);
  void set_position(Vector);
  void set_velocity(Vector);
  void set_color(Color);
  void move(double);
  void apply_friction(double);
  void apply_gravity(double);
  void apply_force(Vector, double);
  void apply_collision_force(const Vector&, double, double);
  double dist(Particle*) const;
  void add_bond(Bond*);
  void remove_bond(Bond*);
  bool has_bond(Particle*) const;
  Bond* get_bond(Particle*);
  const std::set<Bond*>& get_bonds() const;
  Cell* get_cell() const;
  void set_cell(Cell*);
  
  void set_sharp(bool);
  bool is_sharp() const;
  void set_heavy(bool);
  bool is_heavy() const;

private:
  bool _static;
  double mass;
  Vector position;
  Vector velocity;
  Color color;
  std::set<Bond*> bonds;
  Cell* cell = nullptr;
  RigidBody* body = nullptr;
  
  bool sharp;
  bool heavy;
};