#pragma once

class Video {
public:
  Video(const char*, int, int);
  ~Video();
  bool is_valid();
  int get_pixels_num();
  uint8_t* get_imgd();
  void add_frame();
  void finish();

private:
  bool valid;
  const char* pth;
  int width;
  int height;
  int pixels_num;
  uint8_t* imgd;
  FILE* proc;
};